import { Router, IRouter } from 'express';
import Controller from "controller.interface";

class BaseController implements Controller {
  public path: string;

  public router: IRouter = Router();

  constructor(path: string) {
    this.path = path;
  }
}

export default BaseController;
