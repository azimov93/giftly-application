import { NextFunction, Request, Response } from "express";
import StatusCodes from "http-status-codes";
import BaseController from "./base.controller";
import throwError from "../utils/throwError";
import ERRORS from "../config/errors";
import responseHandler from "../utils/responseHandler";
import { getOne, getSearchQuery } from "../database";

class ProductController extends BaseController {
  constructor(baseUrl: string) {
    super(baseUrl);

    this.initRoutes();
  }

  private initRoutes = () => {
    this.router.get(`${this.path}`, this.getList);
    this.router.get(`${this.path}/:id`, this.getOne);
  };

  private getList = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const products = await getSearchQuery("products", req.query);

      return responseHandler(res, StatusCodes.OK, products);
    } catch (error) {
      return throwError(
        ERRORS.SYSTEM.SYSTEM_ERROR,
        StatusCodes.SERVICE_UNAVAILABLE,
        next,
      );
    }
  };

  private getOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const product = await getOne("products", req.params.id);

      return responseHandler(res, StatusCodes.OK, product);
    } catch (error) {
      return throwError(
        ERRORS.SYSTEM.SYSTEM_ERROR,
        StatusCodes.SERVICE_UNAVAILABLE,
        next,
      );
    }
  };
}

export default ProductController;
