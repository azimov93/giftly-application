import { NextFunction, Request, Response } from "express";
import StatusCodes from "http-status-codes";
import BaseController from "./base.controller";
import throwError from "../utils/throwError";
import ERRORS from "../config/errors";
import responseHandler from "../utils/responseHandler";
import { getEntity } from "../database";

class VendorController extends BaseController {
  constructor(baseUrl: string) {
    super(baseUrl);

    this.initRoutes();
  }

  private initRoutes = () => {
    this.router.get(`${this.path}`, this.getList);
  };

  private getList = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const vendors = await getEntity("vendors");

      return responseHandler(res, StatusCodes.OK, vendors);
    } catch (error) {
      return throwError(
        ERRORS.SYSTEM.SYSTEM_ERROR,
        StatusCodes.SERVICE_UNAVAILABLE,
        next,
      );
    }
  };
}

export default VendorController;
