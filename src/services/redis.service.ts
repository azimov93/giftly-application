import redisClient, { RedisClient } from "redis";
import { promisify } from "util";
import CONSTANTS from "../config/constants";

type RedisGetPromise = (key: string) => Promise<string>;
type RedisSetPromise = (
  key: string,
  value: string,
  mode: string,
  duration: number
) => Promise<string>;

interface IRedisConnection extends RedisClient {
  getAsync?: RedisGetPromise;
  setAsync?: RedisSetPromise;
}

const { REDIS } = CONSTANTS.APP;

class RedisService {
  private connection: IRedisConnection;

  constructor(redisDb: number) {
    this.connection = redisClient.createClient({
      url: `${REDIS.HOST}${redisDb}`,
    });

    this.connection.getAsync = promisify(this.connection.get);
    this.connection.setAsync = promisify(this.connection.set);
  }

  getData = async (key: string) => {
    const data = await this.connection.getAsync(key);

    return JSON.parse(data);
  };

  setData = async (
    key: string,
    data: unknown,
    cacheSettings: [string?, number?] = [],
  ) => {
    const dataToJson = JSON.stringify(data);

    return this.connection.setAsync(key, dataToJson, ...cacheSettings);
  };
}

export default RedisService;
