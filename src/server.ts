import "dotenv/config";
import App from "./app";
import ProductController from "./controllers/product.controller";
import VendorController from "./controllers/vendor.controller";
import PromotionController from "./controllers/promotion.controller";
import CONSTANTS from "./config/constants";

const app = new App(
  [
    new ProductController(CONSTANTS.APP.ROUTES.PRODUCTS),
    new VendorController(CONSTANTS.APP.ROUTES.VENDORS),
    new PromotionController(CONSTANTS.APP.ROUTES.PROMOTION),
  ],
);

app.listen();
