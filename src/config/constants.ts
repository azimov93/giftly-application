const {
  PORT,
  REDIS_URL,
  DOMAIN,
} = process.env;

const CONSTANTS = {
  APP: {
    DOMAIN,
    PORT,
    REDIS: {
      HOST: REDIS_URL,
      DB: {
        CACHE: 0,
      },
      CACHE_TIME: 5 * 60
    },
    ROUTES: {
      PRODUCTS: "/products",
      VENDORS: "/vendors",
      PROMOTION: "/promotion"
    }
  },
};

export default CONSTANTS;
