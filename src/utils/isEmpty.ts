const isEmpty = <T>(data: T) => {
  if (!data || typeof data !== 'object') {
    return true;
  }

  const array = Array.isArray(data) ? data : Object.keys(data);

  return array.length === 0;
};

export default isEmpty;
