import { StatusCodes } from "http-status-codes";
import { Response } from "express";

const sendResponse = (res: Response, status: number, data: any) =>
  res.status(status).json(data);

const errorResponse = (res: Response, status: StatusCodes, error: string) => {
  const responseBody = {
    status,
    error: {
      message: error,
    },
  };

  return sendResponse(res, status, responseBody);
};

const successResponse = (res: Response, status: StatusCodes, data: any) => {
  const responseBody = {
    status,
    data,
  };

  return sendResponse(res, status, responseBody);
};

const responseHandler = (
  res: Response,
  status: StatusCodes,
  data: any,
  isError: boolean = false,
) => {
  const handler = isError ? errorResponse : successResponse;

  return handler(res, status, data);
};

export default responseHandler;
