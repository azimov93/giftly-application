import { StatusCodes } from "http-status-codes";
import { NextFunction } from "express";
import HttpException from "../exceptions/HttpException";

const throwError = (
  errorMessage: string,
  errorStatus: StatusCodes,
  next: NextFunction,
) => {
  const err = new HttpException(errorStatus, errorMessage);

  next(err);
};

export default throwError;
