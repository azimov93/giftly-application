import { StatusCodes } from "http-status-codes";

export interface IHttpException extends Error {
  status: StatusCodes;
  message: string;
}

class HttpException extends Error implements IHttpException {
  constructor(public status: number, public message: string) {
    super(message);
  }
}

export default HttpException;
