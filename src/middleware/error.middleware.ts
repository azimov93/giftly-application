import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { IHttpException } from "../exceptions/HttpException";
import responseHandler from "../utils/responseHandler";

const errorMiddleware = (
  error: IHttpException,
  req: Request,
  res: Response,
) => {
  const status =
    error.status === 500 ? StatusCodes.INTERNAL_SERVER_ERROR : error.status;

  return responseHandler(res, status, error.message, true);
};

export default errorMiddleware;
