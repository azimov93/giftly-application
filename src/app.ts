import express, { Application } from "express";
import bodyParser from "body-parser";
import logger from "morgan";
import cookieParser from "cookie-parser";
import helmet from "helmet";
import cors from "cors";
import StatusCodes from "http-status-codes";

import CONSTANTS from "./config/constants";
import HttpException from "./exceptions/HttpException";
import errorMiddleware from "./middleware/error.middleware";
import Controller from "./types/controller.interface";

class App {
  public app: Application;

  constructor(controllers: Controller[]) {
    this.app = express();

    this.startupConfig();
    this.initCORS();
    this.initMiddlewares();
    this.initLogger();
    this.initializeControllers(controllers);
    this.initErrorHandling();
  }

  private startupConfig = () => {
    this.app.disable('x-powered-by');
    this.app.set('etag', false);
  };

  private initMiddlewares() {
    this.app.use(helmet());
    this.app.use(cookieParser());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private initCORS() {
    this.app.use(cors());
  }

  private initErrorHandling() {
    this.app.use((req, res, next) => {
      const err = new HttpException(StatusCodes.NOT_FOUND, "NOT_FOUND");

      next(err);
    });
    this.app.use(errorMiddleware);
  }

  private initLogger() {
    this.app.use(
      logger("dev", {
        skip: () => this.app.get("env") === "test",
      }),
    );
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use("/api", controller.router);
    });
  }

  public listen() {
    const { APP } = CONSTANTS;
    const { PORT = APP.PORT || 8000 } = process.env;

    this.app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
  }
}

export default App;
