import QueryString from "qs";
import filter from "lodash/filter";
import find from "lodash/find";
import RedisService from "../services/redis.service";
import CONSTANTS from "../config/constants";
import data from "./data/data.json";
import isEmpty from "../utils/isEmpty";

const { REDIS } = CONSTANTS.APP;

type Entity = "vendors" | "promotion" | "products";
type MediaType = "video" | "image";

interface ProductMedia {
  id: string;
  type: MediaType;
  url: string;
}

interface Product {
  id: string;
  media: ProductMedia[];
  name: string;
  order: number;
  vendor: string;
}

interface Promotion {
  text: string;
  order: number;
  hide: boolean;
}

type EntityType = Product[] | string[] | Promotion;

const redisService = new RedisService(REDIS.DB.CACHE);

export const getEntity = async (entity: Entity): Promise<EntityType> => Promise.resolve(data[entity] as EntityType);

export const getOne = async (entity: Entity, id: string): Promise<Product> => {
  const product = find(data[entity] as Product[], ["id", id]);

  return Promise.resolve(product);
};

export const getSearchQuery = async (entity: Entity, searchMap: QueryString.ParsedQs = {}): Promise<Product[]> => {
  if (isEmpty(searchMap)) {
    return Promise.resolve(data[entity] as Product[]);
  }

  const cacheKey = `${entity}-${JSON.stringify(searchMap)}`;
  let result = await redisService.getData(cacheKey);

  if (isEmpty(result)) {
    result = filter(data[entity] as Product[], (item: Product) => {
      return Object
        .keys(searchMap)
        .every((key: "name" | "vendor") => {
          const isVendor = key === "vendor";

          return isVendor
            ? item[key] === searchMap[key]
            : item[key]
              .toLowerCase()
              .includes((searchMap[key] as string).toLowerCase())
        });
    });

    await redisService.setData(cacheKey, result, ["EX", REDIS.CACHE_TIME]);
  }

  return result;
};
